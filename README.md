![Hello Everyone](https://i.ibb.co/GVm5YKr/1500x500.jpg)

- 🤗 Hi, I'm Fauzan Aji Prayoga
- 🌱 I’m currently learning Kotlin, React.js, Laravel, Node.js
- 📫 Get in touch with me at fauzanjr1@gmail.com

Check my github [@fauzanajipray](https://github.com/fauzanajipray)

Cheers 😄✌
